# https://keras.io/zh/applications/#vgg16
import os;
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2';
from keras.applications.vgg16 import VGG16, preprocess_input, decode_predictions
from keras.preprocessing import image
import numpy as np
from os import listdir
from os.path import isfile, join

# read all img
path = './img/';
files = [f for f in listdir(path) if isfile(join(path, f))]

# load model
model = VGG16(weights='imagenet', include_top=True)

# predict
for i in range(len(files)):
    img_path = path + files[i];
    img = image.load_img(img_path, target_size=(224, 224));
    x = image.img_to_array(img);
    x = np.expand_dims(x, axis=0);
    x = preprocess_input(x);

    features = model.predict(x)
    print('Predicted:', decode_predictions(features, top=1)[0][0][1], ', file name:', files[i]);