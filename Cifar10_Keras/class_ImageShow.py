import matplotlib.pyplot as plt;
import numpy as np;

class ImageShow(object):
	def __init__(self):
		self.index = 0;
		self.label_dict = {	0:"airplane", 1:"automobile", 2:"bird", 
						3:"cat", 4:"deer", 5:"dog", 
						6:"frog", 7:"horse", 8:"ship", 9:"truck"};

	# single image show
	def plot_image(image):	
		plt.figure(self.index);
		self.index += 1;
		plt.imshow(image, cmap='binary');

	# multi image show
	def plot_images_lables_prediction(self, images, labels, prediction, index, num=10):
		if(num > 25):
			num = 25;
		
		fig = plt.figure(self.index);
		self.index += 1;

		for i in range(0, num):
			subplot = plt.subplot(5,5,i+1);
			subplot.imshow(images[index], cmap='binary');
			title = str('label=') + self.label_dict[labels[index][0]];
			if len(prediction) > 0:
				title += ", predict=" + str(self.label_dict[prediction[index]]);
			subplot.set_title(title, fontsize=10);
			subplot.set_xticks([]);
			subplot.set_yticks([]);
			index += 1;

	# show train history
	def plot_train_history(self, train_history, train, validation):
		plt.figure(self.index);
		self.index += 1;
		plt.plot(train_history.history[train]);
		plt.plot(train_history.history[validation]);
		plt.title('Train history');
		plt.xlabel('Epoch');
		plt.ylabel(train);
		plt.legend(['train', 'validation'], loc='upper left')

	# predicte probability
	def show_Predicted_Probability(self, real_label, predction_label, input_img, predicted_probability, index):
		print('label: ', self.label_dict[real_label[index][0]]);
		plt.figure(self.index);
		self.index += 1;
		plt.imshow(np.reshape(input_img[index], (32,32,3)));
		show_figure();

		for i in range(10):
			print(self.label_dict[i] + ' Probanility:%1.9f' %(predicted_probability[index][i])); 

	# show image
	def show_figure(self):
		plt.show();

