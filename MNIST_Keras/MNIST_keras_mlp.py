import os;
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2';
import numpy as np;
import pandas as pd;
from class_ImageShow import ImageShow;
from class_MNIST import data_, model_, predict_;
np.random.seed(1);



# data_preprocessing
data = data_();
data.data_preprocessing();

# model
model = model_()
model.build_model_mlp();
model.load_model("", "MNIST_mlp.h5");
model.callback_tensorboard("./logs/mlp");
model.train_model(data.train_normalize, data.train_lable_onehot);
model.save_model("", "MNIST_mlp.h5");
model.evaluate_model(data.test_normalize, data.test_lable_onehot);

# predict
predict = predict_();
predict.prediction(model.model, data.test_input);

# result
print(model.model.summary());	
print('accuracy=', model.scores[1]);	
print(predict.predict);	
result = ImageShow();
result.plot_train_history(model.train_history, 'acc', 'val_acc');	
result.plot_train_history(model.train_history, 'loss', 'val_loss');	
result.plot_images_lables_prediction(data.test_image, data.test_label, predict.predict, 340, 25);	
result.show_figure();

# confusion matrix
pd.crosstab(data.test_label, predict.predict, rownames=['label'], colnames=['predict']);
df = pd.DataFrame({'label':data.test_label, 'predict':predict.predict});
print(df[df.label != df.predict]);	

