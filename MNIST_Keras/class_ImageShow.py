import matplotlib.pyplot as plt;
class ImageShow():
	def __init__(self):
		self.index = 0;

	# single image show
	def plot_image(self, image):	
		plt.figure(self.index);
		self.index += 1;
		plt.imshow(image, cmap='binary');

	# multi image show
	def plot_images_lables_prediction(self, images, labels, prediction, index, num=10):
		if(num > 25):
			num = 25;
		
		fig = plt.figure(self.index);
		self.index += 1;

		for i in range(0, num):
			subplot = fig.add_subplot(5,5,i+1);
			subplot.imshow(images[index], cmap='binary');
			title = "lable=" + str(labels[index]);
			if len(prediction) > 0:
				title += ", predict=" + str(prediction[index]);
			subplot.set_title(title, fontsize=10);
			subplot.set_xticks([]);
			subplot.set_yticks([]);
			index += 1;

	# show train history
	def plot_train_history(self, train_history, train, validation):
		plt.figure(self.index);
		self.index += 1;
		plt.plot(train_history.history[train]);
		plt.plot(train_history.history[validation]);
		plt.title('Train history');
		plt.xlabel('Epoch');
		plt.ylabel(train);
		plt.legend(['train', 'validation'], loc='upper left')

	def show_figure(self):
		plt.show();
		