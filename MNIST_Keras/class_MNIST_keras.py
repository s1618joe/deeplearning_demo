import os;
from keras import optimizers;
from keras.utils import np_utils;
from keras.datasets import mnist;
from keras.models import Sequential;
from keras.layers import Dense, Activation, Dropout, Flatten, Conv2D, MaxPooling2D;
from keras.callbacks import EarlyStopping, TensorBoard;



class data_():
	def __init__(self):
		self.train_image = None;
		self.train_input = None;
		self.train_normalize = None;
		self.train_lable = None;
		self.train_lable_onehot = None;

		self.test_image = None;
		self.test_input = None;
		self.test_normalize = None;
		self.test_label = None;
		self.test_lable_onehot = None;

	def data_preprocessing(self):
		# load data
		(self.train_image, self.train_lable), (self.test_image, self.test_label) = mnist.load_data();	# (60000,28,28), (60000,); (10000,28,28), (10000,)
		# reshape
		self.train_input = self.train_image.reshape(60000, 784).astype('float32');	# (60000,784)
		self.test_input = self.test_image.reshape(10000, 784).astype('float32');		# (10000,784)
		# normalize
		self.train_normalize = self.train_input/255;
		self.test_normalize = self.test_input/255;
		# one-hat enconding
		self.train_lable_onehot = np_utils.to_categorical(self.train_lable);	# (60000,10)
		self.test_lable_onehot = np_utils.to_categorical(self.test_label);	# (10000,10)

	def data_preprocessing_cnn(self):
		# load data
		(self.train_image, self.train_lable), (self.test_image, self.test_label) = mnist.load_data();	# (60000,28,28), (60000,); (10000,28,28), (10000,)
		# reshape
		self.train_input = self.train_image.reshape(self.train_image.shape[0], 28, 28, 1).astype('float32');	# (60000,28,28,1)
		self.test_input = self.test_image.reshape(self.test_image.shape[0], 28, 28, 1).astype('float32');		# (10000,28,28,1)
		# normalize
		self.train_normalize = self.train_input/255;
		self.test_normalize = self.test_input/255;
		# one-hat enconding
		self.train_lable_onehot = np_utils.to_categorical(self.train_lable);
		self.test_lable_onehot = np_utils.to_categorical(self.test_label);



class model_():
	def __init__(self):
		self.model = None;
		self.scores = None;
		self.train_history = None;
		self.callback = None;

	def build_model_mlp(self):
		self.model = Sequential();
		self.model.add(Dense(	input_dim=784,	# input size
								units=256,	# layer node
								kernel_initializer='normal',	# initial weight
								activation='relu'));	# Activations Layers
		self.model.add(Dropout(0.2));	# random drop out weight by rate
		self.model.add(Dense(	units=64,	# layer node
								kernel_initializer='normal',	# initial weight
								activation='relu'));	# Activations Layers
		self.model.add(Dropout(0.2));	# random drop out weight by rate
		self.model.add(Dense(	units=10,	# layer node
								kernel_initializer='normal', 	# initial weight
								activation='softmax'));	# Activations Layers
		adam = optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=1e-5, amsgrad=False);	
		self.model.compile(	loss='categorical_crossentropy',	# loss function
							optimizer=adam,	# optimizer methon
							metrics=['accuracy']);	# evaluate metrics

	def build_model_cnn(self):
		self.model = Sequential();
		self.model.add(Conv2D(	filters=16, 	# number of filter
								kernel_size=(5,5), 	# filter size
								padding='same',	# valid, causal, same
								input_shape=(28, 28, 1), 	# input size
								activation='relu'));	#  Activations Layers
		self.model.add(MaxPooling2D(pool_size=(2,2)));	# max-pool size, input_shape(28,28,1), output_shape(14,14,8)
		self.model.add(Conv2D(	filters=32, 	# number of filter
								kernel_size=(3,3), 	# filter size
								padding='same',		# valid, causal, same
								activation='relu'));	#  Activations Layers
		self.model.add(MaxPooling2D(pool_size=(2,2)));	# max-pool size, input_shape(14,14,1), output_shape(7,7,16)
		self.model.add(Dropout(0.25));	# random drop out weight by rate
		self.model.add(Flatten());	# Flattens the input
		self.model.add(Dense(128, activation='relu'));	# layer node
		self.model.add(Dropout(0.25));	# random drop out weight by rate
		self.model.add(Dense(10, activation='softmax'));	# layer node
		adam = optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=1e-5, amsgrad=False);	
		self.model.compile(	loss='categorical_crossentropy',	# loss function
							optimizer=adam,	# optimizer methon
							metrics=['accuracy']);	# evaluate metrics

	def train_model(self, train_normalize, train_lable_onehot):
		self.train_history = self.model.fit(x=train_normalize,	# imput
											y=train_lable_onehot,	# label(one-hot)
											callbacks=self.callback,
											validation_split=0.2,	# validation:train(data) = 2:8
											epochs=10,	# number of iterate
											batch_size=200,	# batch size 
											verbose=2);	# log type 
	
	def callback_EarlyStop(self):
		self.callback = EarlyStopping(	monitor='val_loss', 
										min_delta=1e-5, 	# less than min_delta -> mean no improvement
										patience=3, 		# number of epochs with no improvement after which training will be stopped
										verbose=0, 			
										mode='auto',
										baseline=None, 
										restore_best_weights=False);

	def callback_tensorboard(self, FileName):
		self.callback = TensorBoard(log_dir=FileName,  # log 目录
                 					histogram_freq=0,  # 按照何等频率（epoch）来计算直方图，0为不计算
									write_graph=True,  # 是否存储网络结构图
									write_grads=True, # 是否可视化梯度直方图
									write_images=True,# 是否可视化参数
									embeddings_freq=0, 
									embeddings_layer_names=None, 
									embeddings_metadata=None);
		self.callback = [self.callback];

	def evaluate_model(self, test_normalize, test_lable_onehot):
		self.scores = self.model.evaluate(test_normalize, test_lable_onehot);

	def load_model(self, sPath, sFile):
		if(os.path.isfile(sPath + sFile)):
			self.model.load_weights(sPath + sFile);
		else:
			print("Not find model, train a new one!");

	def save_model(self, sPath, sFile):
		self.model.save_weights(sPath + sFile)
		if(os.path.isfile(sPath + sFile)):
			print("Save model weight success!");
		else:
			print("Save model weight fail!");



class predict_():
	def __init__(self):
		self.predict = None;

	def prediction(self, model, test_input):
		self.predict = model.predict_classes(test_input);		# (10000,)

		
