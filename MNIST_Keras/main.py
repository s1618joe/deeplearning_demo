import sys;
import os;
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2';
import numpy as np;
import pandas as pd;
from class_ImageShow import ImageShow;
from class_MNIST import data_, model_, predict_;
from PIL import Image;
np.random.seed(1);



if __name__ == '__main__':
	# input
	img = Image.open(sys.argv[1]).convert('L');
	array_img = np.array(img);
	input_mlp = array_img.reshape(1, 28*28).astype('float32');
	input_cnn = array_img.reshape(1, 28, 28, 1).astype('float32');

	# mlp 
	model = model_();
	model.build_model_mlp();
	model.load_model("", "MNIST_mlp.h5");
	predict = predict_();
	predict.prediction(model.model, input_mlp);
	print("MLP predict result: ",  predict.predict);

	# cnn
	model_cnn = model_();
	model_cnn.build_model_cnn();
	model_cnn.load_model("", "MNIST_cnn.h5");
	predict_cnn = predict_();
	predict_cnn.prediction(model_cnn.model, input_cnn);
	print("CNN predict result: ",  predict_cnn.predict);
	