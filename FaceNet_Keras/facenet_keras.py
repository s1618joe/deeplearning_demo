# https://makerpro.cc/2018/12/introduction-to-face-recognition-model-google-facenet/
import sys, os, time;
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2';
from os import listdir
from os.path import isfile, join

import numpy as np;
import cv2;
from keras.models import load_model;
from scipy.spatial import distance;
from class_facenet import face_net, webcam, openCV_pen;



if __name__ == '__main__':
	sDetectModelPath = './model/cv2/haarcascade_frontalface_alt2.xml';   # from openCV
	sFaceModelPath = './model/keras/facenet_keras.h5';	# keras
	sMembersPath = './members/';
	sMembersFile = [f.split('.')[0] for f in listdir(sMembersPath) if isfile(join(sMembersPath, f))];

	members = [];
	for x in sMembersFile:
		members.append(x.split('.')[0]);
	feature = np.loadtxt(sMembersPath + "/feature/feature.csv");
	model = load_model(sFaceModelPath);
	FN = face_net(sDetectModelPath);
	WC = webcam(sDetectModelPath);

	if(len(sys.argv) > 1):
		img_input = cv2.imread(sys.argv[1]);
		
		FN.predict_result(img_input, feature, members, model);

		cv2.imshow('Result', img_input);
		cv2.waitKey(0);
		cv2.destroyAllWindows();
	else:
		WC.control_predict(feature, members, model);