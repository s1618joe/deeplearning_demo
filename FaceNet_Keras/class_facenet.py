import numpy as np;
from time import sleep;
import cv2;
from scipy.spatial import distance;
from skimage.transform import resize;

class face_net():
	def __init__(self, sDetectModel):
		self.sDetectModel = sDetectModel;

	#偵測並取得臉孔area，接著再resize為模型要求的尺寸(下方例子並未作alignment)
	def AlignImage(self, img_input, margin, image_size=160):
		Cascade = cv2.CascadeClassifier(self.sDetectModel);
		detect = Cascade.detectMultiScale(
			img_input, 
			scaleFactor=1.1, 
			minNeighbors=5, 
			minSize=(30, 30));

		aligned = [];
		for i in range(len(detect)):
			(x, y, w, h) = detect[i];
			face = img_input[y:y+h, x:x+w];
			faceMargin = np.zeros((h+margin*2, w+margin*2, 3), dtype = 'uint8');
			faceMargin[margin:margin+h, margin:margin+w] = face;
			aligned.append(resize(faceMargin, (image_size, image_size), mode='reflect'));
			cv2.rectangle(img_input, (x,y), (x+w, y+h), (0, 255, 0), 2);

		return (detect, aligned, img_input);

    #圖像的預處理(即前述的幾項步驟)
	def PreProcess(self, img):
	    whitenImg = self.PreWhiten(img);
	    whitenImg = whitenImg[np.newaxis, :];
	    return whitenImg;
		
	#圖像白化（whitening）可用於對過度曝光或低曝光的圖片進行處理，處理的方式就是改變圖像的平均像素值為 0 ，改變圖像的方差為單位方差 1。
	def PreWhiten(self, img_input):
	    if img_input.ndim == 4:
	        axis = (1, 2, 3);
	        size = img_input[0].size;
	    elif img_input.ndim == 3:
	        axis = (0, 1, 2);
	        size = img_input.size;
	    else:
	        raise ValueError('Dimension should be 3 or 4');

	    mean = np.mean(img_input, axis=axis, keepdims=True);
	    std = np.std(img_input, axis=axis, keepdims=True);
	    std_adj = np.maximum(std, 1.0/np.sqrt(size));
	    img_output = (img_input - mean) / std_adj;
	    return img_output;


	#使用L1或L2標準化圖像，可強化其特徵。
	def l2_normalize(self, x, axis=-1, epsilon=1e-10):
	    return x / np.sqrt(np.maximum(np.sum(np.square(x), axis=axis, keepdims=True), epsilon));

	def predict_result(self, img_input, feature, members, model):
		(detect, aligned, img_output) = self.AlignImage(img_input, 6);
		
		for i in range(len(aligned)):
			faceImg = self.PreProcess(aligned[i]);
			embs_valid = self.l2_normalize(np.concatenate(model.predict(faceImg)));

			score = [];
			for j in range(len(feature)):
				score.append(distance.euclidean(embs_valid, feature[j]));

			if score != []:
				predict_result = members[score.index(min(score))] + ", " + str(round(min(score), 4));
				img_input = openCV_pen.Set(img_input, predict_result, (detect[i][0] + detect[i][2], detect[i][1] + detect[i][3]));
				print(predict_result);

		return img_output;
		


class webcam():
	def __init__(self, sDetectModel, iCamNum=0, bCamColor=True):
		self.iCamNum = iCamNum;
		self.bCamStatus = False;
		self.bCamColor = bCamColor;
		self.iCamLoadTimes = 0;
		self.sDetectModel = sDetectModel;

	def CheckCamConnect(self, video_capture, iCount=3):
		# check camera connected
		while True:
			if self.iCamLoadTimes >= iCount:
				print('Please check your camera!');
				break;
			elif not video_capture.isOpened():
				print('Unable to load camera.');
				sleep(3);
				self.iCamLoadTimes += 1;
				pass;
			else:
				self.bCamStatus = True;
				print('key "q" is quit.');
				break;

	def control_detect(self, sSavePath="./members/data/", bDetect=True, bSave=True):
		video_capture = cv2.VideoCapture(self.iCamNum);
		self.CheckCamConnect(video_capture);
		FD = face_detect(self.sDetectModel);		

		while self.bCamStatus:
			# capture frame-by-frame
			ref, imgFrame = video_capture.read();
			
			# convert RGB to Gray
			if not self.bCamColor:
				imgFrame = cv2.cvtColor(imgFrame, cv2.COLOR_BGR2GRAY);

			# detect
			if bDetect:
				imgFrame = FD.detect(imgFrame, sSavePath, bSave);

			# show video
			cv2.imshow('Video', imgFrame);

			# break while loop
			if cv2.waitKey(1) & 0xFF == ord('q'):
				break;

		# When everything is done, release the capture
		video_capture.release();
		cv2.destroyAllWindows();

	def control_predict(self, feature, members, model):
		video_capture = cv2.VideoCapture(self.iCamNum);
		self.CheckCamConnect(video_capture);
		FN = face_net(self.sDetectModel);

		while self.bCamStatus:
			# capture frame-by-frame
			ref, imgFrame = video_capture.read();

			# convert RGB to Gray
			if not self.bCamColor:
				imgFrame = cv2.cvtColor(imgFrame, cv2.COLOR_BGR2GRAY);

			# predict
			imgFrame = FN.predict_result(imgFrame, feature, members, model);

			# show video
			cv2.imshow('Video', imgFrame);

			# break while loop
			if cv2.waitKey(1) & 0xFF == ord('q'):
				break;

		# When everything is done, release the capture
		video_capture.release();
		cv2.destroyAllWindows();



class face_detect():
	def __init__(self, sDetectModel):
		self.iCount = 0;
		self.sDetectModel = sDetectModel;

	def detect(self, imgInput, sSavePath="./members/", bSave=True):
		margin = 20;
		img = imgInput.copy();

		# face detect
		Cascade = cv2.CascadeClassifier(self.sDetectModel);
		detect = Cascade.detectMultiScale(
			img, 
			scaleFactor=1.1, 
			minNeighbors=5, 
			minSize=(30, 30)
		);

		#draw a rectangle around the faces
		for (x, y, w, h) in detect:
			cv2.rectangle(img, (x,y), (x+w, y+h), (0, 255, 0), 2);
			
			if bSave:
				img_cut = imgInput[y-margin:y+h+margin, x-margin:x+w+margin].copy();
				self.iCount += 1;
				file = str(self.iCount) + '_.jpg';
				cv2.imwrite(sSavePath + file, img_cut);
				print("Save!");

		return img;


		
class openCV_pen():
	def Set(img, text, org, font=cv2.FONT_HERSHEY_DUPLEX, fontScale=0.7, fontcolor=(0,255,0), lineSize=1, leneType=cv2.LINE_AA):
		cv2.putText(img, text, org, font, fontScale, fontcolor, lineSize, leneType);
		return img;