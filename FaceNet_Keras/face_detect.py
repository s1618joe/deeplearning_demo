import cv2;
import sys;
from class_facenet import face_detect, webcam;



if __name__ == '__main__':
	sDetectModelPath = './model/cv2/haarcascade_frontalface_alt2.xml';
	sImgSavePath = "./members/data/";

	FD = face_detect(sDetectModelPath);
	WC = webcam(sDetectModelPath);

	if len(sys.argv) > 1:
		img = cv2.imread(sys.argv[1]);
		img = FD.detect(img, sImgSavePath, bSave=True);
		
		cv2.imshow('Result', img)
		cv2.waitKey(0);
		cv2.destroyAllWindows();
	else:
		WC.control_detect(sImgSavePath);