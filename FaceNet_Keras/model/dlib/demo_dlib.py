from imutils import face_utils;
import dlib;
import cv2;
import sys;
 
# Vamos inicializar um detector de faces (HOG) para então
# let's go code an faces detector(HOG) and after detect the 
# landmarks on this detected face

# p = our pre-treined model directory, on my case, it's on the same script's diretory.
sDetectModelPath = '';#'./model/dlib/';
sModelFile = "shape_predictor_68_face_landmarks.dat";
p = sDetectModelPath + sModelFile;
detector = dlib.get_frontal_face_detector();
predictor = dlib.shape_predictor(p);

if __name__ == '__main__':

    if len(sys.argv) > 1:
        img = cv2.imread(sys.argv[1]);

        # face detect
        #rects = detector(img, 2);
        rects, scores, idx = detector.run(img, 0, 0)
        # For each detected face, find the landmark.
        for (i, d) in enumerate(rects):
            #Draw rectangle
            x1 = d.left()
            y1 = d.top()
            x2 = d.right()
            y2 = d.bottom()
            text = "%2.2f(%d)" % (scores[i], idx[i])
            cv2.rectangle(img, (x1, y1), (x2, y2), (0, 255, 0), 4, cv2.LINE_AA)
            cv2.putText(img, text, (x1, y1), cv2.FONT_HERSHEY_DUPLEX, 
                0.7, (255, 255, 255), 2, cv2.LINE_AA);


            # Make the prediction and transfom it to numpy array
            shape = predictor(img, d)
            shape = face_utils.shape_to_np(shape)
            # Draw on our image, all the finded cordinate points (x,y) 
            for (x, y) in shape:
                cv2.circle(img, (x, y), 2, (0, 255, 0), -1);

        cv2.imshow('Result', img)
        cv2.waitKey(0);

    else:
        cap = cv2.VideoCapture(0)
 
        while True:
            # Getting out image by webcam 
            _, image = cap.read()
                
            # Get faces into webcam's image
            rects, scores, idx = detector.run(image, 0, 0)

            # mark
            for (i, d) in enumerate(rects):
                #Draw rectangle
                x1 = d.left()
                y1 = d.top()
                x2 = d.right()
                y2 = d.bottom()
                text = "%2.2f(%d)" % (scores[i], idx[i])
                cv2.rectangle(image, (x1, y1), (x2, y2), (0, 255, 0), 4, cv2.LINE_AA)
                cv2.putText(image, text, (x1, y1), cv2.FONT_HERSHEY_DUPLEX, 
                    0.7, (255, 255, 255), 2, cv2.LINE_AA);
            
            # For each detected face, find the landmark.
            for (i, rect) in enumerate(rects):
                # Make the prediction and transfom it to numpy array
                shape = predictor(image, rect)
                shape = face_utils.shape_to_np(shape)
            
                # Draw on our image, all the finded cordinate points (x,y) 
                for (x, y) in shape:
                    cv2.circle(image, (x, y), 2, (0, 255, 0), -1)
            
            # Show the image
            cv2.imshow("Output", image)
            
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        cap.release()
        cv2.destroyAllWindows()