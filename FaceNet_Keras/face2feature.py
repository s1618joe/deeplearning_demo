import sys, os, time;
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2';
from os import listdir;
from os.path import isfile, join;

import numpy as np;
import cv2;
from keras.models import load_model;
from class_facenet import face_net;



sDetectModelPath = './model/cv2/haarcascade_frontalface_alt2.xml';
sFaceModelPath = './model/keras/facenet_keras.h5';
sMembersPath = "./members/";
sMembersFile = [f for f in listdir(sMembersPath) if isfile(join(sMembersPath, f))];

FN = face_net(sDetectModelPath);
model = load_model(sFaceModelPath);

feature = [];
for member in sMembersFile:
	img = cv2.imread(sMembersPath + member);
	(detect, aligned, img) = FN.AlignImage(img, 6);

	for i in range(len(aligned)):
		faceImg = FN.PreProcess(aligned[i]);
		embs = FN.l2_normalize(np.concatenate(model.predict(faceImg)));
		feature.append(embs);
		print(member + ".");

np.savetxt(sMembersPath+"feature/feature.csv", feature);


